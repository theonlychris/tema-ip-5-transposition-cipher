#include <iostream>
#include <set> 
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <algorithm>

/* Overloads the << Operator to show the contents of a matrix */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<std::vector<T>>& myMatrix)
{
	for (int i = 0; i < myMatrix.size(); ++i)
	{
		for (int j = 0; j < myMatrix[i].size(); ++j)
		{
			os << myMatrix[i][j] << " ";
		}
		os << std::endl;
	}
	return os;
}

void TransportationCipher()
{

	std::string text;
	std::cout << "Input text: " << std::endl;
	std::getline(std::cin, text);

	std::string key;
	std::cout << "Input key: " << std::endl;
	std::getline(std::cin, key);

	
	std::set<char> keySet;
	std::string keyToUpper = key;
	std::transform(keyToUpper.begin(), keyToUpper.end(), keyToUpper.begin(), ::toupper);

	/* Verifies if the key is valid */
	for (int i = 0; i < key.size(); ++i)
	{
		if (keySet.insert(keyToUpper[i]).second == false)
		{
			std::cout << "Cheia invalida";
			exit(-1);
		}
	}

	//std::cout << keyToUpper << std::endl;
	//std::cout << key << std::endl;
	//std::cout << text << std::endl;

	/* Erases the whitespaces */
	text.erase(std::remove_if(text.begin(), text.end(), ::isspace), text.end());

	//std::cout << text << std::endl;

	int nrColumns = key.size();
	int	nrLines = std::ceil((float)text.size() / nrColumns);
	//std::cout << nrColumns << nrLines;

	char toFill = 'a';
	std::vector<std::vector<char>> matrix(nrLines, std::vector<char>(nrColumns));

	int textIndex = 0;
	for (int i = 0; i < matrix.size(); ++i)
	{
		for (int j = 0; j < matrix[i].size(); ++j)
		{
			if (textIndex > text.size() - 1)
				matrix[i][j] = toFill++;
			else
			matrix[i][j] = text[textIndex++];
		}
	}
			

	std::cout << matrix;

	//std::vector<std::pair<char, int>> cryptOrder;
	std::map<char, int> cryptOrder;
	for (int i = 0; i < key.size(); ++i)
		cryptOrder.insert(std::make_pair(key[i], i));

	std::string keyOrder = key;
	std::sort(keyOrder.begin(), keyOrder.end());

	std::string cryptedText;
	std::string decryptedText;
	textIndex = 0;

	/* Encryption */
	while (textIndex < keyOrder.size())
	{
	int currentColumn = cryptOrder.find(keyOrder[textIndex++])->second;

		for (int i = 0; i < nrLines; ++i)
			cryptedText += matrix[i][currentColumn];
	}
	std::cout << "Crypted text: " << cryptedText << std::endl;

	/* Decryption */
	int counter = 0;

	for (int i = 0; i < nrLines; ++i)
		for (int j = 0; j < nrColumns; ++j)
		{
			if (counter < text.size())
			{
				counter++;
				decryptedText += matrix[i][j];
			}
			else
				break;
		}

	std::cout << "Decrypted text: " << decryptedText << std::endl;




}


int main()
{
	TransportationCipher();
	std::cin.get();
	return 0;
}